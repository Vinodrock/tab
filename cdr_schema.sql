-- MariaDB dump 10.19  Distrib 10.5.12-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: asteriskcdrdb
-- ------------------------------------------------------
-- Server version	10.5.12-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdr` (
  `calldate` datetime NOT NULL DEFAULT '1000-01-01 00:00:00',
  `clid` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `src` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `dst` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `dcontext` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `channel` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `dstchannel` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lastapp` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `lastdata` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `duration` int(11) NOT NULL DEFAULT 0,
  `billsec` int(11) NOT NULL DEFAULT 0,
  `disposition` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `amaflags` int(11) NOT NULL DEFAULT 0,
  `accountcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `uniqueid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `userfield` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `did` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `recordingfile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cnum` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cnam` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `outbound_cnum` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `outbound_cnam` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `dst_cnam` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `linkedid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `peeraccount` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sequence` int(11) NOT NULL DEFAULT 0,
  KEY `calldate` (`calldate`),
  KEY `dst` (`dst`),
  KEY `accountcode` (`accountcode`),
  KEY `uniqueid` (`uniqueid`),
  KEY `did` (`did`),
  KEY `recordingfile` (`recordingfile`),
  KEY `linkedid` (`linkedid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `cdr_bi` BEFORE INSERT ON `cdr` FOR EACH ROW
BEGIN

  IF (NEW.lastapp = 'Playback' OR NEW.lastapp = 'BackGround' OR NEW.lastapp = 'AGI' OR NEW.lastapp = 'Hangup' OR NEW.lastapp = 'Queue' OR NEW.lastapp = 'Dial') THEN

    IF NEW.uniqueid NOT IN (SELECT uniqueid FROM `asteriskcdrdb`.`cdr` WHERE `uniqueid` = NEW.uniqueid AND `lastapp` IN ('Playback','BackGround','AGI','Hangup','Queue','Dial')) THEN 

      INSERT INTO `phonikip_db`.`tbl_ivr_event` (calldate, duration, lastapp, did, uniqueid, lastdata, src, billsec, recordingfile, disposition, channel)
      VALUES (new.calldate, new.duration, new.lastapp, new.did, new.uniqueid, new.lastdata, new.src, new.billsec, new.recordingfile, new.disposition, new.channel);

    ELSE

      UPDATE `phonikip_db`.`tbl_ivr_event` SET `billsec` = new.billsec, `duration` =  new.duration, `disposition` =  new.disposition WHERE `uniqueid` = NEW.uniqueid AND `lastapp` = new.lastapp;

    END IF;

    END IF;

  

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `cel`
--

DROP TABLE IF EXISTS `cel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventtype` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `eventtime` datetime NOT NULL,
  `cid_name` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid_num` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid_ani` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid_rdnis` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid_dnid` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exten` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channame` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appname` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appdata` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amaflags` int(11) NOT NULL,
  `accountcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uniqueid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `linkedid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `peer` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userdeftype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniqueid_index` (`uniqueid`),
  KEY `linkedid_index` (`linkedid`),
  KEY `context_index` (`context`)
) ENGINE=InnoDB AUTO_INCREMENT=7224758 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tbl_ami_events`
--

DROP TABLE IF EXISTS `tbl_ami_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ami_events` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Cre_datetime` datetime DEFAULT NULL,
  `Event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Linkedid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_channel` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_caller_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_callerid_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ConnectedLineNum` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Exten` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_context` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uniqueid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_channel` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_caller_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_callerid_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_context` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_uniqueid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=110826 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tbl_ami_events_ai` AFTER INSERT ON `tbl_ami_events` FOR EACH ROW
BEGIN
		DECLARE ring_sec varchar(30);
		DECLARE hold_sec varchar(30);
		DECLARE check_acw_st varchar(30);
		DECLARE check_rec_exsit varchar(30);
		DECLARE check_status varchar(30);
		DECLARE check_BF varchar(30);
		DECLARE check_ob_status varchar(30);
		DECLARE get_ob_status_id INT;
		DECLARE get_userid INT;
		DECLARE dest_cal_Length INT;
		DECLARE outoacw_sec_count INT;
		DECLARE endpoint_spilt varchar(30);
 		DECLARE check_autoacw_st varchar(30);
                DECLARE getprvcall varchar(30);


		
		IF NEW.Event = 'DialBegin' THEN

				Update asterisk.ps_contacts SET 
							status = "Ringing",
							status_des = "Call",
							linkedid = NEW.Linkedid
							where endpoint=NEW.Dest_caller_id;

			IF NEW.Frm_context = 'ext-queues' or NEW.Frm_context = 'from-blind-transfer' THEN

				INSERT INTO phonikip_db.tbl_calls_evnt SET 
									call_type = "Inbound",
									frm_caller_num = NEW.Frm_caller_id,
									uniqueid = NEW.uniqueid,
									linkedid = NEW.Linkedid,
									agnt_queueid = NEW.Exten,
									did_num = NEW.Dest_caller_id,
									date = CURDATE(),
									cre_datetime = NOW(),
									status = "Ringing";

			ELSEIF NEW.Frm_context = 'macro-dialout-trunk' or NEW.Frm_context = 'from-blind-transfer' THEN

				 SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				 SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

				Update asterisk.ps_contacts SET 
						status = "Dialing",
						status_des = "Call",
						update_datetime= NOW(),
						linkedid= NEW.Linkedid
						where endpoint=@endpoint_spilt;
						
				SET @get_userid	 := (SELECT asterisk.ps_contacts.userid AS userid
											FROM
											asterisk.ps_contacts
											where endpoint = @endpoint_spilt);

				INSERT INTO phonikip_db.tbl_calls_evnt SET 
									call_type = "Outbound",
									to_caller_num = NEW.Dest_caller_id,
									uniqueid = NEW.uniqueid,
									linkedid = NEW.Linkedid,
									date = CURDATE(),
									cre_datetime = NOW(),
									dest_uniqueid = NEW.Dest_uniqueid,
									agnt_userid = @get_userid,
									agnt_sipid = @endpoint_spilt,
									status = "Ringing";

			ELSEIF NEW.Frm_context = 'macro-dial-one' THEN

					Update asterisk.ps_contacts SET 
									status = "Dialing",
									status_des = "Call",
									update_datetime= NOW(),
									linkedid = NEW.Linkedid
									where endpoint=NEW.frm_caller_id;

					IF NEW.Dest_callerid_name = NEW.Dest_caller_id  THEN
						
				

						SET @check_rec_exsit := (SELECT phonikip_db.tbl_calls_evnt.linkedid AS linkedid
										FROM
										phonikip_db.tbl_calls_evnt
										where linkedid=NEW.Linkedid limit 1);

							SET @get_userid := (SELECT asterisk.ps_contacts.userid AS userid
												FROM
												asterisk.ps_contacts
												where endpoint = NEW.Frm_caller_id);

							
								SET @get_userid := (SELECT asterisk.ps_contacts.userid AS userid
													FROM
													asterisk.ps_contacts
													where endpoint = NEW.Frm_caller_id);
								INSERT INTO phonikip_db.tbl_calls_evnt SET 
													call_type = "Internalcall",
													frm_caller_num = NEW.Dest_caller_id,
													uniqueid = NEW.uniqueid,
													linkedid = NEW.Linkedid,
													date = CURDATE(),
													cre_datetime = NOW(),
													dest_uniqueid = NEW.Dest_uniqueid,
													agnt_userid = @get_userid,
													agnt_sipid = NEW.Frm_caller_id,
													status = "Ringing";
							

						
						
					ELSE
						SET @get_userid := (SELECT asterisk.ps_contacts.userid AS userid
												FROM
												asterisk.ps_contacts
												where endpoint = NEW.Dest_caller_id);

							Update phonikip_db.tbl_calls_evnt SET 
										dest_uniqueid = NEW.uniqueid,
										agnt_userid = @get_userid,
										agnt_sipid = NEW.Dest_caller_id
										where linkedid=NEW.Linkedid Order by id desc limit 1;	
						
					
						
				      END IF;

			END IF;


		ELSEIF NEW.Event = 'DialEnd' THEN
			 IF NEW.Status = 'ANSWER' THEN

					SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  				 	SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
					Update asterisk.ps_contacts SET 
								status = "In Call",
								status_des = "Call",
								linkedid = NEW.Linkedid,
								update_datetime= NOW()
								where endpoint=@endpoint_spilt AND Status!='ACW';
			
			
				
				END IF;	

			IF NEW.Frm_context = 'ext-queues' or NEW.Frm_context = 'from-blind-transfer' THEN

				SET @outoacw_sec_count := (SELECT phonikip_db.tbl_acw_time.acw_sec AS acw_sec
											FROM
											phonikip_db.tbl_acw_time
											INNER JOIN phonikip_db.tbl_agnt_queue_types ON phonikip_db.tbl_agnt_queue_types.type_name = phonikip_db.tbl_acw_time.agnt_queue_typeid
											and phonikip_db.tbl_agnt_queue_types.queueid = phonikip_db.tbl_acw_time.queueid

											INNER JOIN phonikip_db.tbl_calls_evnt ON phonikip_db.tbl_calls_evnt.agnt_userid = phonikip_db.tbl_agnt_queue_types.userid
											and phonikip_db.tbl_calls_evnt.agnt_queueid = phonikip_db.tbl_agnt_queue_types.queueid
											where phonikip_db.tbl_calls_evnt.linkedid=NEW.Linkedid Order by phonikip_db.tbl_calls_evnt.id desc limit 1);

				SET @get_call_date := (SELECT phonikip_db.tbl_calls_evnt.cre_datetime AS cre_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
				IF NEW.Status = 'ANSWER' THEN
					Update phonikip_db.tbl_calls_evnt SET 
										ring_sec_count = @ring_sec,
										answer_datetime = NOW(),
										outoacw_sec_count = @outoacw_sec_count,
										status = NEW.Status
										where linkedid=NEW.Linkedid   Order by id desc limit 1;
				ELSE
					Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
				END IF;

			ELSEIF (NEW.Frm_context = 'macro-dialout-trunk' OR NEW.Frm_context = 'macro-dial-one') THEN

				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

						Update asterisk.ps_contacts SET 
								status = "Out Call",
								status_des = "Call",
								linkedid = NEW.Linkedid,
								update_datetime= NOW()
								where endpoint=@endpoint_spilt;

				SET @outoacw_sec_count := (SELECT phonikip_db.tbl_acw_time.acw_sec AS acw_sec
											FROM
											phonikip_db.tbl_acw_time
											INNER JOIN phonikip_db.tbl_agnt_queue_types ON phonikip_db.tbl_agnt_queue_types.type_name = phonikip_db.tbl_acw_time.agnt_queue_typeid
											and phonikip_db.tbl_agnt_queue_types.queueid = phonikip_db.tbl_acw_time.queueid

											INNER JOIN phonikip_db.tbl_calls_evnt ON phonikip_db.tbl_calls_evnt.agnt_userid = phonikip_db.tbl_agnt_queue_types.userid
											and phonikip_db.tbl_calls_evnt.agnt_queueid = phonikip_db.tbl_agnt_queue_types.queueid
											where phonikip_db.tbl_calls_evnt.linkedid=NEW.Linkedid Order by phonikip_db.tbl_calls_evnt.id desc limit 1);
				SET @get_call_date := (SELECT phonikip_db.tbl_calls_evnt.cre_datetime AS cre_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
					IF NEW.Status = 'ANSWER' THEN
						Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											answer_datetime = NOW(),
											outoacw_sec_count = @outoacw_sec_count,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
					ELSE
						Update phonikip_db.tbl_calls_evnt SET 
											ring_sec_count = @ring_sec,
											status = NEW.Status
											where linkedid=NEW.Linkedid Order by id desc limit 1;
					END IF;
			END IF;

		ELSEIF NEW.Event = 'Hangup' THEN

		

			IF ( NEW.Frm_context = 'ext-queues' or NEW.Frm_context = 'from-queue-custom') THEN

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where linkedid=NEW.Linkedid Order by id desc limit 1;

			ELSEIF (NEW.Frm_context = 'from-internal' OR NEW.Frm_context = 'ext-local' OR NEW.Frm_context = 'macro-dialout-trunk' ) THEN
			

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid and answer_datetime !="" Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where linkedid=NEW.Linkedid Order by id desc limit 1;
			ELSEIF (NEW.Frm_context = 'from-conf') THEN
			

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where (dest_uniqueid = NEW.uniqueid or uniqueid = NEW.uniqueid)  Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where (dest_uniqueid = NEW.uniqueid or uniqueid = NEW.uniqueid) Order by id desc limit 1;

			ELSEIF (NEW.Frm_context = 'macro-dial-one') THEN
			

				SET @get_anscall_date := (SELECT phonikip_db.tbl_calls_evnt.answer_datetime AS answer_datetime
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid and agnt_sipid=NEW.Frm_caller_id Order by id desc limit 1);

				SET @ring_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_anscall_date , NOW()));

				Update phonikip_db.tbl_calls_evnt SET 
									answer_sec_count = @ring_sec,
									hangup_datatime = NOW()
									where linkedid=NEW.Linkedid Order by id desc limit 1;
			END IF;



			SET @check_acw_st := (SELECT phonikip_db.tbl_acw_tmp.linkid AS linkid
									FROM
									phonikip_db.tbl_acw_tmp
									where linkid=NEW.Linkedid);

			SET @check_autoacw_st := (SELECT phonikip_db.tbl_calls_evnt.outoacw_sec_count AS outoacw_sec_count
									FROM
									phonikip_db.tbl_calls_evnt
									where linkedid=NEW.Linkedid and status='ANSWER' order by id desc limit 1);



			IF @check_acw_st != '' THEN

				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));		

					Update asterisk.ps_contacts SET 
									status = "ACW",
									status_des = "",
									linkedid = "",
									update_datetime= NOW()
									where endpoint=@endpoint_spilt;

					



			ELSEIF @check_autoacw_st != '' THEN
				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));		
					IF LENGTH(@endpoint_spilt) > 4 THEN
						SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  						SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
					END IF;
					Update asterisk.ps_contacts SET 
									status = "ACW",
									status_des = "",
									linkedid = "",
									update_datetime= NOW()
									where endpoint=@endpoint_spilt;


			ELSE

				SET @check_status := (SELECT phonikip_db.tbl_calls_evnt.status AS status
												FROM
												phonikip_db.tbl_calls_evnt
												where linkedid=NEW.Linkedid Order by id desc limit 1);

				SET @check_BF := (SELECT phonikip_db.tbl_calls_evnt.desc
												FROM
												phonikip_db.tbl_calls_evnt
												where linkedid=NEW.Linkedid Order by id desc limit 1);
				
				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  				SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

				SET @get_ob_status_id := (SELECT tbl_agnt_evnt.id as partone_id
														FROM
														phonikip_db.tbl_agnt_evnt
														where agnt_event='Outbound On' AND agnt_sipid =@endpoint_spilt Order by id desc limit 1);

				SET @check_ob_status := (SELECT tbl_agnt_evnt.id
														FROM
														phonikip_db.tbl_agnt_evnt
														where id_of_prtone=@get_ob_status_id AND agnt_sipid =@endpoint_spilt );  
				
				IF (@check_ob_status IS NULL and @get_ob_status_id != '') THEN

						
						Update asterisk.ps_contacts SET 
										status = "Outbound",
										status_des = "",
										linkedid = "",
										update_datetime= NOW()
										where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid ;
				ELSE
					SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  					SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
					IF (@check_status != 'ANSWER' OR @check_BF = 'BlindTransfer') THEN
						SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  						SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
						
						SET @getprvcall := (SELECT phonikip_db.tbl_calls_evnt.Linkedid AS Linkedid
																		FROM
																		phonikip_db.tbl_calls_evnt
																		where agnt_sipid=@endpoint_spilt and status='ANSWER' and answer_sec_count is null Order by id desc limit 1);
								
							IF  @getprvcall != '' THEN
							Update asterisk.ps_contacts SET 
												status = "Busy",
												status_des = "Call",
												linkedid = @getprvcall,
												update_datetime= NOW()
												where endpoint=@endpoint_spilt;
							ELSE
									Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid;
							END IF;
					ELSE
						IF  (NEW.Frm_context = 'from-internal' or NEW.Frm_context = 'from-conf') THEN
							SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  							SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));

							SET @getprvcall := (SELECT phonikip_db.tbl_calls_evnt.Linkedid AS Linkedid
																		FROM
																		phonikip_db.tbl_calls_evnt
																		where agnt_sipid=@endpoint_spilt and status='ANSWER' and answer_sec_count is null Order by id desc limit 1);
								
							IF  @getprvcall != '' THEN
							Update asterisk.ps_contacts SET 
												status = "Busy",
												status_des = "Call",
												linkedid = @getprvcall,
												update_datetime= NOW()
												where endpoint=@endpoint_spilt;
							ELSE
									Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid;
							END IF;
							
							
							IF @endpoint_spilt != NEW.Frm_caller_id THEN
								SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '@', 1));
  								SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
							END IF;
							Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid ;
						ELSE
							Update asterisk.ps_contacts SET 
												status = "Online",
												status_des = "",
												linkedid = "",
												update_datetime= NOW()
												where endpoint=@endpoint_spilt or linkedid = NEW.Linkedid ;

						END IF;
					END IF;
				END IF;
			END IF;
			

		ELSEIF NEW.Event = 'Hold' THEN
			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
			Update asterisk.ps_contacts SET 
							status = "Busy",
							status_des = "Hold",
                                                        linkedid=NEW.Linkedid
							where endpoint=@endpoint_spilt;

			INSERT INTO phonikip_db.tbl_calls_hold_evnts SET 
							uniqueid = NEW.uniqueid,
							linkedid = NEW.Linkedid,
							date = CURDATE(),
							hold_datetime = NOW();
							
		ELSEIF NEW.Event = 'BlindTransfer' THEN
			SET @get_userid	 := (SELECT asterisk.ps_contacts.userid AS userid
											FROM
											asterisk.ps_contacts
											where endpoint = NEW.Frm_caller_id);
			
			Update phonikip_db.tbl_calls_evnt SET 
									`desc` = 'BlindTransfer',
									`agnt_sipid` = NEW.Frm_caller_id,
									`agnt_userid` = @get_userid,
									`to_trans_no` = NEW.Exten,
									`to_trans_linkedid` = NEW.Linkedid
									where linkedid=NEW.Linkedid Order by id desc limit 1;

		ELSEIF NEW.Event = 'Unhold' THEN
			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(NEW.Frm_channel, '-', 1));
  			SET @endpoint_spilt := (SELECT SUBSTRING_INDEX(@endpoint_spilt, '/', -1));
			Update asterisk.ps_contacts SET 
					status = "Busy",
					status_des = "Call",
                                        linkedid=NEW.Linkedid
					where endpoint=@endpoint_spilt;

			SET @get_call_date := (SELECT phonikip_db.tbl_calls_hold_evnts.hold_datetime AS hold_datetime
									FROM
									phonikip_db.tbl_calls_hold_evnts
									where linkedid=NEW.Linkedid Order by id desc limit 1);

			SET @hold_sec := (SELECT TIMESTAMPDIFF(SECOND, @get_call_date , NOW()));
			Update phonikip_db.tbl_calls_hold_evnts SET 
							`unhold_datatime` = NOW(),
							`hold_sec_count` = @hold_sec
							where linkedid=NEW.Linkedid Order by id desc limit 1;
		END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tbl_ami_events_temp`
--

DROP TABLE IF EXISTS `tbl_ami_events_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ami_events_temp` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Cre_datetime` datetime DEFAULT NULL,
  `Event` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Linkedid` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_channel` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_caller_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_callerid_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ConnectedLineNum` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Exten` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Frm_context` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uniqueid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_channel` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_caller_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_callerid_name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_context` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Dest_uniqueid` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Status` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8826 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'asteriskcdrdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-15 17:13:00
